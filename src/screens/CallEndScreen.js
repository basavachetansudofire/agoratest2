import React, {Component} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';

import {S} from '../css/commonStyles';
import {texts} from '../css/textStyles';
import {Icon} from 'react-native-elements'

class CallEndScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={[S.hp100wp100, {backgroundColor: '#fff'}]}>
        <Icon raised name='close' type='simple-line-icon' color='black' size={98} />
        <Text style={texts.b48}>Call Ended</Text>
      </View>
    );
  }
}

export default CallEndScreen;
