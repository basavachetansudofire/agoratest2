// Cut this Code
import React, {Component} from 'react';
import {
  Platform,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  PermissionsAndroid,
  Dimensions,
  StyleSheet,
  Image,
} from 'react-native';
import RtcEngine, {
  RtcLocalView,
  RtcRemoteView,
  VideoRenderMode,
} from 'react-native-agora';

import {S} from '../css/commonStyles';
import {texts} from '../css/textStyles';
import {Icon} from 'react-native-elements';

class RecieveScreen extends Component {
  _engine; //: RtcEngine (MODIFIED)

  constructor(props) {
    super(props);
    this.state = {
      appId: 'a2777faa1e6447f49eb95847f1511e24',
      // APP ID from Agora Console
      channelName: 'channel-x',
      // Channel Name (Our Input)
      token:
        '006a2777faa1e6447f49eb95847f1511e24IAD6gfcfdx9TELyoD7zLZ7Z7GXQ541xwzBkESIwn6l8wkAJkFYoAAAAAEAD+bihb3N9JYQEAAQDa30lh',
      // Token is generated via
      // Secure Server in production and Temp Tokens
      // can be generated from Agora Console for 24 Hours
      joinSucceed: false,
      // This decides to view the video of the user or not
      peerIds: [],
      // Array of the IDs of all the users joined
      callOnGoing: false,
      otherUserJoined: false,
    };
  }

  componentDidMount() {
    this.init();
    //Initialising the Agora RTC engine
    this.requestCameraAndAudioPermission();
    // Requesting Camera and Audio Permissions (Android)
  }

  requestCameraAndAudioPermission = async () => {
    try {
      const granted = await PermissionsAndroid.requestMultiple([
        PermissionsAndroid.PERMISSIONS.CAMERA,
        PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
      ]);
      if (
        granted['android.permission.RECORD_AUDIO'] ===
          PermissionsAndroid.RESULTS.GRANTED &&
        granted['android.permission.CAMERA'] ===
          PermissionsAndroid.RESULTS.GRANTED
      ) {
        console.log('You can use the cameras & mic');
      } else {
        console.log('Permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  };

  // Pass in your App ID through this.state, create and initialize an RtcEngine object.
  init = async () => {
    const {appId} = this.state;
    // Creating Agora Instance
    this._engine = await RtcEngine.create(appId);

    // Enable the video module.
    await this._engine.enableVideo();

    // Listen for the UserJoined callback.
    // This callback occurs when the remote user successfully joins the channel.
    this._engine.addListener('UserJoined', (uid, elapsed) => {
      console.log('UserJoined', uid, elapsed);
      const {peerIds} = this.state;
      if (peerIds.indexOf(uid) === -1) {
        this.setState({
          peerIds: [...peerIds, uid],
        });
      }
      this.setState({otherUserJoined: true});
      console.log('Other User Joined');
    });

    // Listen for the UserOffline callback.
    // This callback occurs when the remote user leaves the channel or drops offline.
    this._engine.addListener('UserOffline', (uid, reason) => {
      console.log('UserOffline', uid, reason);
      const {peerIds} = this.state;
      this.setState({
        // Remove peer ID from state array
        peerIds: peerIds.filter(id => id !== uid),
      });
      this.endCall()
      this.props.navigation.navigate('CallEnd')
    });

    // Listen for the JoinChannelSuccess callback.
    // This callback occurs when the local user successfully joins the channel.
    this._engine.addListener('JoinChannelSuccess', (channel, uid, elapsed) => {
      console.log('JoinChannelSuccess', channel, uid, elapsed);
      this.setState({
        joinSucceed: true,
      });
    });
  };

  startCall = async () => {
    await this._engine?.joinChannel(
      this.state.token,
      this.state.channelName,
      null,
      0,
    );
    // if (!this.state.otherUserJoined) {
    //   setTimeout(this.endCall, 10000);
    // }
    this.setState({callOnGoing: true});
    console.log('callOnGoing');
  };

  _renderVideos = () => {
    const {joinSucceed} = this.state;
    return joinSucceed ? (
      <View style={styles.fullView}>{this._renderRemoteVideos()}</View>
    ) : null;
  };

  _renderRemoteVideos = () => {
    const {peerIds} = this.state;
    // console.log('Size of Peer ID', peerIds.length)
    return (
      <View style={styles.fullView}>
        {peerIds.length > 0 ? (
          <View style={{height: '100%', width: '100%'}}>
            <RtcLocalView.SurfaceView
              style={styles.max}
              channelId={this.state.channelName}
              renderMode={VideoRenderMode.Hidden}
            />
            {/* {this._renderRemoteVideos()} */}
            <ScrollView
              style={styles.remoteContainer}
              contentContainerStyle={{paddingHorizontal: 2.5}}
              horizontal={true}>
              {peerIds.map((value, index, array) => {
                return (
                  <RtcRemoteView.SurfaceView
                    style={styles.remote}
                    uid={value}
                    channelId={this.state.channelName}
                    renderMode={VideoRenderMode.Hidden}
                    zOrderMediaOverlay={true}
                  />
                );
              })}
            </ScrollView>
          </View>
        ) : (
          <View
            style={{
              height: '100%',
              width: '100%',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <View
              style={{
                // height: 200,
                width: 300,
                backgroundColor: '#999',
                elevation: 5,
              }}>
              <Text
                style={{
                  fontSize: 48,
                  fontWeight: 'bold',
                }}>
                Connecting...
              </Text>
            </View>
          </View>
        )}
        
        <TouchableOpacity
          onPress={() => {
            this.endCall()
            this.props.navigation.navigate('CallEnd');
          }}
          style={[
            S.h60,
            S.w60,
            S.br30elev,
            S.abs,
            {left: 170, bottom: 80, backgroundColor: 'green'},
          ]}>
          <Icon
            name="close"
            type="simple-line-icon"
            color="#40E0D0"
            size={24}
          />
        </TouchableOpacity>
      </View>
    );
  };

  endCall = async () => {
    await this._engine?.leaveChannel();
    this.setState({peerIds: [], joinSucceed: false});
    this.setState({callOnGoing: false});
    console.log('Call Ended');
  };

  render() {
    return (
      <View style={{flex: 1, alignItems: 'center', backgroundColor: '#fff'}}>
        <View style={styles.max}>
          <View style={styles.max}>
            {/* <View style={styles.buttonHolder}>
              <TouchableOpacity onPress={this.startCall} style={styles.button}>
                <Text style={styles.buttonText}> Start Call </Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={this.endCall} style={styles.button}>
                <Text style={styles.buttonText}> End Call </Text>
              </TouchableOpacity>
            </View> */}
            {!this.state.callOnGoing && (
              <View style={[S.hp100wp100, {backgroundColor: '#D7E9F7'}]}>
                <Text style={[texts.b32, {color: '#3F0071'}]}>Call From</Text>
                <Image
                  source={{
                    uri: 'https://randomuser.me/api/portraits/men/75.jpg',
                  }}
                  style={{
                    height: 120,
                    width: 120,
                    borderRadius: 60,
                    marginVertical: 20,
                  }}
                />
                <Text style={[texts.b48, S.mb20, {color: '#79B4B7'}]}>
                  John Smith
                </Text>
                <Icon
                  onPress={this.startCall}
                  raised
                  name="phone-incoming"
                  type="material-community"
                  color="#290FBA"
                  size={64}
                />
              </View>
            )}
            {this._renderVideos()}
          </View>
        </View>
      </View>
    );
  }
}

export default RecieveScreen;

const dimensions = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};

const styles = StyleSheet.create({
  max: {
    flex: 1,
    width: '100%',
  },
  buttonHolder: {
    height: 100,
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  button: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    backgroundColor: '#0093E9',
    borderRadius: 25,
  },
  buttonText: {
    color: '#fff',
  },
  fullView: {
    width: dimensions.width,
    height: dimensions.height,
  },
  remoteContainer: {
    width: '100%',
    height: 150,
    position: 'absolute',
    top: 5,
  },
  remote: {
    width: 150,
    height: 150,
    marginHorizontal: 2.5,
  },
  noUserText: {
    paddingHorizontal: 10,
    paddingVertical: 5,
    color: '#0093E9',
  },
});
