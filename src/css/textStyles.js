import {StyleSheet} from 'react-native';

export const texts = StyleSheet.create({
    // EXTRA STYLES

    // Grey Color 
    gc: {
      color: '#666'
    },
  
    // Text Align 
    tac: {
      textAlign: 'center',
    },
    taj: {
      textAlign: 'justify',
    },

    // UnderLine 
    tu: {
        textDecorationLine: 'underline'
    },
  
    // Black Font Styles
    b8: {
      fontFamily: 'Poppins-Medium',
      fontSize: 8,
    },
    b9: {
      fontFamily: 'Poppins-Medium',
      fontSize: 9,
    },
    b10: {
      fontFamily: 'Poppins-Medium',
      fontSize: 10,
    },
    b11: {
      fontFamily: 'Poppins-Medium',
      fontSize: 11,
    },
    b12: {
      fontFamily: 'Poppins-Medium',
      fontSize: 12,
    },
    b13: {
      fontFamily: 'Poppins-Medium',
      fontSize: 13,
    },
    b14: {
      fontFamily: 'Poppins-Medium',
      fontSize: 14,
    },
    b15: {
      fontFamily: 'Poppins-Medium',
      fontSize: 15,
    },
    b16: {
      fontFamily: 'Poppins-Medium',
      fontSize: 16,
    },
    b17: {
      fontFamily: 'Poppins-Medium',
      fontSize: 17,
    },
    b18: {
      fontFamily: 'Poppins-Medium',
      fontSize: 18,
    },
    b19: {
      fontFamily: 'Poppins-Medium',
      fontSize: 19,
    },
    b20: {
      fontFamily: 'Poppins-Medium',
      fontSize: 20,
    },
    b22: {
      fontFamily: 'Poppins-Medium',
      fontSize: 22,
    },
    b24: {
      fontFamily: 'Poppins-Medium',
      fontSize: 24,
    },
    b28: {
      fontFamily: 'Poppins-Medium',
      fontSize: 28,
    },
    b32: {
      fontFamily: 'Poppins-Medium',
      fontSize: 32,
    },
    b48: {
      fontFamily: 'Poppins-Medium',
      fontSize: 48,
    },
  
    // White Font Styles
    w8: {
      fontFamily: 'Poppins-Medium',
      fontSize: 8,
      color: 'white',
    },
    w9: {
      fontFamily: 'Poppins-Medium',
      fontSize: 9,
      color: 'white',
    },
    w10: {
      fontFamily: 'Poppins-Medium',
      fontSize: 10,
      color: 'white',
    },
    w11: {
      fontFamily: 'Poppins-Medium',
      fontSize: 11,
      color: 'white',
    },
    w12: {
      fontFamily: 'Poppins-Medium',
      fontSize: 12,
      color: 'white',
    },
    w13: {
      fontFamily: 'Poppins-Medium',
      fontSize: 13,
      color: 'white',
    },
    w14: {
      fontFamily: 'Poppins-Medium',
      fontSize: 14,
      color: 'white',
    },
    w15: {
      fontFamily: 'Poppins-Medium',
      fontSize: 15,
      color: 'white',
    },
    w16: {
      fontFamily: 'Poppins-Medium',
      fontSize: 16,
      color: 'white',
    },
    w17: {
      fontFamily: 'Poppins-Medium',
      fontSize: 17,
      color: 'white',
    },
    w18: {
      fontFamily: 'Poppins-Medium',
      fontSize: 18,
      color: 'white',
    },
    w19: {
      fontFamily: 'Poppins-Medium',
      fontSize: 19,
      color: 'white',
    },
    w20: {
      fontFamily: 'Poppins-Medium',
      fontSize: 20,
      color: 'white',
    },
    w22: {
      fontFamily: 'Poppins-Medium',
      fontSize: 22,
      color: 'white',
    },
    w24: {
      fontFamily: 'Poppins-Medium',
      fontSize: 24,
      color: 'white',
    },
    w28: {
      fontFamily: 'Poppins-Medium',
      fontSize: 28,
      color: 'white',
    },
    w32: {
      fontFamily: 'Poppins-Medium',
      fontSize: 32,
      color: 'white',
    },
    w48: {
      fontFamily: 'Poppins-Medium',
      fontSize: 48,
      color: 'white',
    },
  
    // Color Font Styles
    c8: {
      fontFamily: 'Poppins-Medium',
      fontSize: 8,
      color: '#1C3A5D',
    },
    c9: {
      fontFamily: 'Poppins-Medium',
      fontSize: 9,
      color: '#1C3A5D',
    },
    c10: {
      fontFamily: 'Poppins-Medium',
      fontSize: 10,
      color: '#1C3A5D',
    },
    c11: {
      fontFamily: 'Poppins-Medium',
      fontSize: 11,
      color: '#1C3A5D',
    },
    c12: {
      fontFamily: 'Poppins-Medium',
      fontSize: 12,
      color: '#1C3A5D',
    },
    c13: {
      fontFamily: 'Poppins-Medium',
      fontSize: 13,
      color: '#1C3A5D',
    },
    c14: {
      fontFamily: 'Poppins-Medium',
      fontSize: 14,
      color: '#1C3A5D',
    },
    c15: {
      fontFamily: 'Poppins-Medium',
      fontSize: 15,
      color: '#1C3A5D',
    },
    c16: {
      fontFamily: 'Poppins-Medium',
      fontSize: 16,
      color: '#1C3A5D',
    },
    c17: {
      fontFamily: 'Poppins-Medium',
      fontSize: 17,
      color: '#1C3A5D',
    },
    c18: {
      fontFamily: 'Poppins-Medium',
      fontSize: 18,
      color: '#1C3A5D',
    },
    c19: {
      fontFamily: 'Poppins-Medium',
      fontSize: 19,
      color: '#1C3A5D',
    },
    c20: {
      fontFamily: 'Poppins-Medium',
      fontSize: 20,
      color: '#1C3A5D',
    },
    c22: {
      fontFamily: 'Poppins-Medium',
      fontSize: 22,
      color: '#1C3A5D',
    },
    c24: {
      fontFamily: 'Poppins-Medium',
      fontSize: 24,
      color: '#1C3A5D',
    },
    c28: {
      fontFamily: 'Poppins-Medium',
      fontSize: 28,
      color: '#1C3A5D',
    },
    c32: {
      fontFamily: 'Poppins-Medium',
      fontSize: 32,
      color: '#1C3A5D',
    },
    c48: {
      fontFamily: 'Poppins-Medium',
      fontSize: 48,
      color: '#1C3A5D',
    },
  });