import {StyleSheet} from 'react-native';
import {Platform} from 'react-native';
export const S = StyleSheet.create({
  // Common Style File

  // Help with Developement
  // Creates a tint and gives a border
  t: {
    borderWidth: 1,
    backgroundColor: '#eee',
  },

  // Extras
  flx: {
    flex: 1,
  },
  abs: {
    position: 'absolute',
  },

  // Divider
  dividerThick100: {
    height: 6,
    width: '100%',
    backgroundColor: '#EEE',
  },
  dividerThick90: {
    height: 6,
    width: '90%',
    backgroundColor: '#666',
  },
  dividerMedium100: {
    height: 4,
    width: '100%',
    backgroundColor: '#F0F0F0',
  },
  dividerMedium90: {
    height: 4,
    width: '90%',
    backgroundColor: '#F0F0F0',
  },
  dividerThin100: {
    height: 2,
    width: '100%',
    backgroundColor: '#F0F0F0',
  },
  dividerThin90: {
    height: 2,
    width: '90%',
    backgroundColor: '#F0F0F0',
  },

  defaultScreen: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
  },

  // Heights and Width
  //   HEIGHT
  // Height with No Width

  //   Height from 5 to 100
  h5: {
    height: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h10: {
    height: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h15: {
    height: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h20: {
    height: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h25: {
    height: 25,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h30: {
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h35: {
    height: 35,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h40: {
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h45: {
    height: 45,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h50: {
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h55: {
    height: 55,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h60: {
    height: 60,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h65: {
    height: 65,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h70: {
    height: 70,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h75: {
    height: 75,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h80: {
    height: 80,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h85: {
    height: 85,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h90: {
    height: 90,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h95: {
    height: 95,
    alignItems: 'center',
    justifyContent: 'center',
  },

  // Heights from 100 to 200
  h100: {
    height: 100,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h110: {
    height: 110,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h120: {
    height: 120,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h130: {
    height: 130,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h140: {
    height: 140,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h150: {
    height: 150,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h160: {
    height: 160,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h170: {
    height: 170,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h180: {
    height: 180,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h190: {
    height: 190,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h200: {
    height: 200,
    alignItems: 'center',
    justifyContent: 'center',
  },

  // Heights from 200 to 300
  h200: {
    height: 200,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h210: {
    height: 210,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h220: {
    height: 220,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h230: {
    height: 230,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h240: {
    height: 240,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h250: {
    height: 250,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h260: {
    height: 260,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h270: {
    height: 270,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h280: {
    height: 280,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h290: {
    height: 290,
    alignItems: 'center',
    justifyContent: 'center',
  },

  // Heights from 300 to 400
  h300: {
    height: 300,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h330: {
    height: 330,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h320: {
    height: 320,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h330: {
    height: 330,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h340: {
    height: 340,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h350: {
    height: 350,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h360: {
    height: 360,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h370: {
    height: 370,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h380: {
    height: 380,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h390: {
    height: 390,
    alignItems: 'center',
    justifyContent: 'center',
  },
  h400: {
    height: 400,
    alignItems: 'center',
    justifyContent: 'center',
  },

  // Heights in Percentages
  hp5: {
    height: '5%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp10: {
    height: '10%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp15: {
    height: '15%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp20: {
    height: '20%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp25: {
    height: '25%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp30: {
    height: '30%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp35: {
    height: '35%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp40: {
    height: '40%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp45: {
    height: '45%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp50: {
    height: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp55: {
    height: '55%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp60: {
    height: '60%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp65: {
    height: '65%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp70: {
    height: '70%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp75: {
    height: '75%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp80: {
    height: '80%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp85: {
    height: '85%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp90: {
    height: '90%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp95: {
    height: '95%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp100: {
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },

  // Heights with Width 100%
  // Heights from 5 to 100
  h5wp100: {
    height: 5,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h10wp100: {
    height: 10,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h15wp100: {
    height: 15,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h20wp100: {
    height: 20,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h25wp100: {
    height: 25,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h30wp100: {
    height: 30,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h35wp100: {
    height: 35,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h40wp100: {
    height: 40,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h45wp100: {
    height: 45,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h50wp100: {
    height: 50,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h55wp100: {
    height: 55,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h60wp100: {
    height: 60,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h65wp100: {
    height: 65,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h70wp100: {
    height: 70,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h75wp100: {
    height: 75,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h80wp100: {
    height: 80,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h85wp100: {
    height: 85,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h90wp100: {
    height: 90,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h95wp100: {
    height: 95,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },

  // Heights from 100 to 200
  h100wp100: {
    height: 100,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h110wp100: {
    height: 110,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h120wp100: {
    height: 120,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h130wp100: {
    height: 130,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h140wp100: {
    height: 140,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h150wp100: {
    height: 150,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h160wp100: {
    height: 160,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h170wp100: {
    height: 170,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h180wp100: {
    height: 180,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h190wp100: {
    height: 190,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h200wp100: {
    height: 200,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },

  // Heights from 200 to 300
  h200wp100: {
    height: 200,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h210wp100: {
    height: 210,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h220wp100: {
    height: 220,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h230wp100: {
    height: 230,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h240wp100: {
    height: 240,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h250wp100: {
    height: 250,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h260wp100: {
    height: 260,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h270wp100: {
    height: 270,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h280wp100: {
    height: 280,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h290wp100: {
    height: 290,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },

  // Heights from 300 to 400
  h300wp100: {
    height: 300,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h310wp100: {
    height: 310,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h320wp100: {
    height: 320,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h330wp100: {
    height: 330,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h340wp100: {
    height: 340,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h350wp100: {
    height: 350,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h360wp100: {
    height: 360,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h370wp100: {
    height: 370,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h380wp100: {
    height: 380,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h390wp100: {
    height: 390,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h400wp100: {
    height: 400,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },

  // Heights in Percentages
  hp5wp100: {
    height: '5%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp10wp100: {
    height: '10%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp15wp100: {
    height: '15%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp20wp100: {
    height: '20%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp25wp100: {
    height: '25%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp30wp100: {
    height: '30%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp35wp100: {
    height: '35%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp40wp100: {
    height: '40%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp45wp100: {
    height: '45%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp50wp100: {
    height: '50%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp55wp100: {
    height: '55%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp60wp100: {
    height: '60%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp65wp100: {
    height: '65%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp70wp100: {
    height: '70%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp75wp100: {
    height: '75%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp80wp100: {
    height: '80%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp85wp100: {
    height: '85%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp90wp100: {
    height: '90%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp95wp100: {
    height: '95%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp100wp100: {
    height: '100%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },

  //   Heights with 50% Width
  //   Heights from 5 to 100
  h5wp50: {
    height: 5,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h10wp50: {
    height: 10,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h15wp50: {
    height: 15,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h20wp50: {
    height: 20,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h25wp50: {
    height: 25,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h30wp50: {
    height: 30,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h35wp50: {
    height: 35,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h40wp50: {
    height: 40,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h45wp50: {
    height: 45,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h50wp50: {
    height: 50,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h55wp50: {
    height: 55,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h60wp50: {
    height: 60,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h65wp50: {
    height: 65,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h70wp50: {
    height: 70,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h75wp50: {
    height: 75,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h80wp50: {
    height: 80,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h85wp50: {
    height: 85,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h90wp50: {
    height: 90,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h95wp50: {
    height: 95,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },

  // Heights from 100 to 200
  h100wp50: {
    height: 100,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h110wp50: {
    height: 110,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h120wp50: {
    height: 120,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h130wp50: {
    height: 130,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h140wp50: {
    height: 140,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h150wp50: {
    height: 150,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h160wp50: {
    height: 160,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h170wp50: {
    height: 170,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h180wp50: {
    height: 180,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h190wp50: {
    height: 190,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h200wp50: {
    height: 200,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },

  // Heights from 200 to 300
  h200wp50: {
    height: 200,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h210wp50: {
    height: 210,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h220wp50: {
    height: 220,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h230wp50: {
    height: 230,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h240wp50: {
    height: 240,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h250wp50: {
    height: 250,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h260wp50: {
    height: 260,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h270wp50: {
    height: 270,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h280wp50: {
    height: 280,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h290wp50: {
    height: 290,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },

  // Heights from
  h300wp50: {
    height: 300,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h310wp50: {
    height: 310,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h320wp50: {
    height: 320,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h330wp50: {
    height: 330,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h340wp50: {
    height: 340,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h350wp50: {
    height: 350,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h360wp50: {
    height: 360,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h370wp50: {
    height: 370,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h380wp50: {
    height: 380,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h390wp50: {
    height: 390,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h400wp50: {
    height: 400,
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },

  // Heights in Percentages
  hp5wp50: {
    height: '5%',
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp10wp50: {
    height: '10%',
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp15wp50: {
    height: '15%',
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp20wp50: {
    height: '20%',
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp25wp50: {
    height: '25%',
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp30wp50: {
    height: '30%',
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp35wp50: {
    height: '35%',
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp40wp50: {
    height: '40%',
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp45wp50: {
    height: '45%',
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp50wp50: {
    height: '50%',
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp55wp50: {
    height: '55%',
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp60wp50: {
    height: '60%',
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp65wp50: {
    height: '65%',
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp70wp50: {
    height: '70%',
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp75wp50: {
    height: '75%',
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp80wp50: {
    height: '80%',
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp85wp50: {
    height: '85%',
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp90wp50: {
    height: '90%',
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp95wp50: {
    height: '95%',
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hp100wp50: {
    height: '100%',
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },

  //   WIDTH
  //   Width from 5 to 100
  w5: {
    width: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w10: {
    width: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w15: {
    width: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w20: {
    width: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w25: {
    width: 25,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w30: {
    width: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w35: {
    width: 35,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w40: {
    width: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w45: {
    width: 45,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w50: {
    width: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w55: {
    width: 55,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w60: {
    width: 60,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w65: {
    width: 65,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w70: {
    width: 70,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w75: {
    width: 75,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w80: {
    width: 80,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w85: {
    width: 85,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w90: {
    width: 90,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w95: {
    width: 95,
    alignItems: 'center',
    justifyContent: 'center',
  },

  // widths from 100 to 200
  w100: {
    width: 100,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w110: {
    width: 110,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w120: {
    width: 120,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w130: {
    width: 130,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w140: {
    width: 140,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w150: {
    width: 150,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w160: {
    width: 160,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w170: {
    width: 170,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w180: {
    width: 180,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w190: {
    width: 190,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w200: {
    width: 200,
    alignItems: 'center',
    justifyContent: 'center',
  },

  // widths from 200 to 300
  w200: {
    width: 200,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w210: {
    width: 210,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w220: {
    width: 220,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w230: {
    width: 230,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w240: {
    width: 240,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w250: {
    width: 250,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w260: {
    width: 260,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w270: {
    width: 270,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w280: {
    width: 280,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w290: {
    width: 290,
    alignItems: 'center',
    justifyContent: 'center',
  },

  // widths from 300 to 400
  w300: {
    width: 300,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w330: {
    width: 330,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w320: {
    width: 320,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w330: {
    width: 330,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w340: {
    width: 340,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w350: {
    width: 350,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w360: {
    width: 360,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w370: {
    width: 370,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w380: {
    width: 380,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w390: {
    width: 390,
    alignItems: 'center',
    justifyContent: 'center',
  },
  w400: {
    width: 400,
    alignItems: 'center',
    justifyContent: 'center',
  },

  // widths in Percentages
  wp5: {
    width: '5%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  wp10: {
    width: '10%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  wp15: {
    width: '15%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  wp20: {
    width: '20%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  wp25: {
    width: '25%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  wp30: {
    width: '30%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  wp35: {
    width: '35%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  wp40: {
    width: '40%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  wp45: {
    width: '45%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  wp50: {
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  wp55: {
    width: '55%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  wp60: {
    width: '60%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  wp65: {
    width: '65%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  wp70: {
    width: '70%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  wp75: {
    width: '75%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  wp80: {
    width: '80%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  wp85: {
    width: '85%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  wp90: {
    width: '90%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  wp95: {
    width: '95%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  wp100: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },

  // Templates
  cc: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  start: {
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },

  // With FlexDirection: 'row'
  rcenter: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  rstart: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  rend: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  rbetween: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  raround: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  revenly: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },

  // With FlexDirection: 'column '
  cstart: {
    justifyContent: 'flex-start',
  },
  cend: {
    justifyContent: 'flex-end',
  },
  cbetween: {
    justifyContent: 'space-between',
  },
  caround: {
    justifyContent: 'space-around',
  },
  cevenly: {
    justifyContent: 'space-evenly',
  },

  //   Align Items
  astart: {
    alignItems: 'flex-start',
  },
  acenter: {
    alignItems: 'center',
  },
  aend: {
    alignItems: 'flex-end',
  },

  //   Padding and Margin

  //   Padding
  //   Padding Overall
  p2: {
    padding: 2,
  },
  p3: {
    padding: 3,
  },
  p5: {
    padding: 5,
  },
  p10: {
    padding: 10,
  },
  p15: {
    padding: 15,
  },
  p20: {
    padding: 20,
  },
  p25: {
    padding: 25,
  },
  p30: {
    padding: 30,
  },
  p40: {
    padding: 40,
  },
  p50: {
    padding: 50,
  },

  //   Padding Top
  pt2: {
    paddingTop: 2,
  },
  pt3: {
    paddingTop: 3,
  },
  pt5: {
    paddingTop: 5,
  },
  pt10: {
    paddingTop: 10,
  },
  pt15: {
    paddingTop: 15,
  },
  pt20: {
    paddingTop: 20,
  },
  pt25: {
    paddingTop: 25,
  },
  pt30: {
    paddingTop: 30,
  },
  pt40: {
    paddingTop: 40,
  },
  pt50: {
    paddingTop: 50,
  },

  //   Padding Bottom
  pb2: {
    paddingBottom: 2,
  },
  pb3: {
    paddingBottom: 3,
  },
  pb5: {
    paddingBottom: 5,
  },
  pb10: {
    paddingBottom: 10,
  },
  pb15: {
    paddingBottom: 15,
  },
  pb20: {
    paddingBottom: 20,
  },
  pb25: {
    paddingBottom: 25,
  },
  pb30: {
    paddingBottom: 30,
  },
  pb40: {
    paddingBottom: 40,
  },
  pb50: {
    paddingBottom: 50,
  },

  // Padding Left
  pl2: {
    paddingLeft: 2,
  },
  pl3: {
    paddingLeft: 3,
  },
  pl5: {
    paddingLeft: 5,
  },
  pl10: {
    paddingLeft: 10,
  },
  pl15: {
    paddingLeft: 15,
  },
  pl20: {
    paddingLeft: 20,
  },
  pl25: {
    paddingLeft: 25,
  },
  pl30: {
    paddingLeft: 30,
  },
  pl40: {
    paddingLeft: 40,
  },
  pl50: {
    paddingLeft: 50,
  },

  // Padding Right
  pr2: {
    paddingRight: 2,
  },
  pr3: {
    paddingRight: 3,
  },
  pr5: {
    paddingRight: 5,
  },
  pr10: {
    paddingRight: 10,
  },
  pr15: {
    paddingRight: 15,
  },
  pr20: {
    paddingRight: 20,
  },
  pr25: {
    paddingRight: 25,
  },
  pr30: {
    paddingRight: 30,
  },
  pr40: {
    paddingRight: 40,
  },
  pr50: {
    paddingRight: 50,
  },

  // Padding Horizontal
  ph2: {
    paddingHorizontal: 2,
  },
  ph3: {
    paddingHorizontal: 3,
  },
  ph5: {
    paddingHorizontal: 5,
  },
  ph10: {
    paddingHorizontal: 10,
  },
  ph15: {
    paddingHorizontal: 15,
  },
  ph20: {
    paddingHorizontal: 20,
  },
  ph25: {
    paddingHorizontal: 25,
  },
  ph30: {
    paddingHorizontal: 30,
  },
  ph40: {
    paddingHorizontal: 40,
  },
  ph50: {
    paddingHorizontal: 50,
  },

  // Padding Vertical
  pv2: {
    paddingVertical: 2,
  },
  pv3: {
    paddingVertical: 3,
  },
  pv5: {
    paddingVertical: 5,
  },
  pv10: {
    paddingVertical: 10,
  },
  pv15: {
    paddingVertical: 15,
  },
  pv20: {
    paddingVertical: 20,
  },
  pv25: {
    paddingVertical: 25,
  },
  pv30: {
    paddingVertical: 30,
  },
  pv40: {
    paddingVertical: 40,
  },
  pv50: {
    paddingVertical: 50,
  },

  //   Margin

  // Margin Overall
  m2: {
    margin: 2,
  },
  m3: {
    margin: 3,
  },
  m5: {
    margin: 5,
  },
  m10: {
    margin: 10,
  },
  m15: {
    margin: 15,
  },
  m20: {
    margin: 20,
  },
  m25: {
    margin: 25,
  },
  m30: {
    margin: 30,
  },
  m40: {
    margin: 40,
  },
  m50: {
    margin: 50,
  },

  //   margin Top
  mt2: {
    marginTop: 2,
  },
  mt3: {
    marginTop: 3,
  },
  mt5: {
    marginTop: 5,
  },
  mt10: {
    marginTop: 10,
  },
  mt15: {
    marginTop: 15,
  },
  mt20: {
    marginTop: 20,
  },
  mt25: {
    marginTop: 25,
  },
  mt30: {
    marginTop: 30,
  },
  mt40: {
    marginTop: 40,
  },
  mt50: {
    marginTop: 50,
  },

  //   margin Bottom
  mb2: {
    marginBottom: 2,
  },
  mb3: {
    marginBottom: 3,
  },
  mb5: {
    marginBottom: 5,
  },
  mb10: {
    marginBottom: 10,
  },
  mb15: {
    marginBottom: 15,
  },
  mb20: {
    marginBottom: 20,
  },
  mb25: {
    marginBottom: 25,
  },
  mb30: {
    marginBottom: 30,
  },
  mb40: {
    marginBottom: 40,
  },
  mb50: {
    marginBottom: 50,
  },

  // margin Left
  ml2: {
    marginLeft: 2,
  },
  ml3: {
    marginLeft: 3,
  },
  ml5: {
    marginLeft: 5,
  },
  ml10: {
    marginLeft: 10,
  },
  ml15: {
    marginLeft: 15,
  },
  ml20: {
    marginLeft: 20,
  },
  ml25: {
    marginLeft: 25,
  },
  ml30: {
    marginLeft: 30,
  },
  ml40: {
    marginLeft: 40,
  },
  ml50: {
    marginLeft: 50,
  },

  // margin Right
  mr2: {
    marginRight: 2,
  },
  mr3: {
    marginRight: 3,
  },
  mr5: {
    marginRight: 5,
  },
  mr10: {
    marginRight: 10,
  },
  mr15: {
    marginRight: 15,
  },
  mr20: {
    marginRight: 20,
  },
  mr25: {
    marginRight: 25,
  },
  mr30: {
    marginRight: 30,
  },
  mr40: {
    marginRight: 40,
  },
  mr50: {
    marginRight: 50,
  },

  // margin Horizontal
  mh2: {
    marginHorizontal: 2,
  },
  mh3: {
    marginHorizontal: 3,
  },
  mh5: {
    marginHorizontal: 5,
  },
  mh10: {
    marginHorizontal: 10,
  },
  mh15: {
    marginHorizontal: 15,
  },
  mh20: {
    marginHorizontal: 20,
  },
  mh25: {
    marginHorizontal: 25,
  },
  mh30: {
    marginHorizontal: 30,
  },
  mh40: {
    marginHorizontal: 40,
  },
  mh50: {
    marginHorizontal: 50,
  },

  // margin Vertical
  mv2: {
    marginVertical: 2,
  },
  mv3: {
    marginVertical: 3,
  },
  mv5: {
    marginVertical: 5,
  },
  mv10: {
    marginVertical: 10,
  },
  mv15: {
    marginVertical: 15,
  },
  mv20: {
    marginVertical: 20,
  },
  mv25: {
    marginVertical: 25,
  },
  mv30: {
    marginVertical: 30,
  },
  mv40: {
    marginVertical: 40,
  },
  mv50: {
    marginVertical: 50,
  },

  // Border Radius, Border Width and Elevation

  // Border Width
  bw: {
    borderWidth: 1,
  },
  bw2: {
    borderWidth: 1,
  },

  // With Elevation
  elev: {
    backgroundColor: 'white',
    elevation: 5,
  },
  br2elev: {
    borderRadius: 2,
    backgroundColor: 'white',
    elevation: 5,
  },
  br5elev: {
    borderRadius: 5,
    backgroundColor: 'white',
    elevation: 5,
  },
  br10elev: {
    borderRadius: 10,
    backgroundColor: 'white',
    elevation: 5,
  },
  br15elev: {
    borderRadius: 15,
    backgroundColor: 'white',
    elevation: 5,
  },
  br20elev: {
    borderRadius: 20,
    backgroundColor: 'white',
    elevation: 5,
  },
  br25elev: {
    borderRadius: 25,
    backgroundColor: 'white',
    elevation: 5,
  },
  br30elev: {
    borderRadius: 30,
    backgroundColor: 'white',
    elevation: 5,
  },

  // Without Elevation
  br2: {
    borderRadius: 2,
  },
  br5: {
    borderRadius: 5,
  },
  br10: {
    borderRadius: 10,
  },
  br15: {
    borderRadius: 15,
  },
  br20: {
    borderRadius: 20,
  },
  br25: {
    borderRadius: 25,
  },
  br30: {
    borderRadius: 30,
  },
});
