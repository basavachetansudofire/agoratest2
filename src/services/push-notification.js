import PushNotification, {Importance} from 'react-native-push-notification';
import { Platform } from 'react-native'

export function setupPushNotification(handleNotification) {
    PushNotification.configure({
  
        onNotification: function(notification) {
          handleNotification(notification)
        },
  
        popInitialNotification: true,
        requestPermissions: Platform.OS === 'ios',
    })
  
    return PushNotification
  }
  