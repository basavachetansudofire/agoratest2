import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import VideoCallScreen from './src/screens/VideoCallScreen';
import RecieveScreen from './src/screens/RecieveScreen';
import HomeScreen from './src/screens/HomeScreen';
import CallEndScreen from './src/screens/CallEndScreen';

const Stack = createNativeStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="CallEnd" component={CallEndScreen} />
        <Stack.Screen name="VideoCall" component={VideoCallScreen} />
        <Stack.Screen name="Recieve" component={RecieveScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
